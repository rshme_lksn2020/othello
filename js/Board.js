class Board{
    constructor(){
        this.board = []
        this.size = 8

        this.drawBoard()
    }

    drawBoard(){
        board.style.display = 'grid'
        board.style.gridTemplateRows = `repeat(${this.size}, auto)`
        board.style.gridGap = '12px'

        for(let i = 0; i < this.size; i++){
            let rows = []
            let row = document.createElement('div')
            row.style.display = 'grid'
            row.style.gridTemplateColumns = `repeat(${this.size}, auto)`
            row.style.gridGap = '12px';
            row.classList.add('row')
            row.setAttribute('data-row', i)

            for(let j = 0; j < this.size; j++) {
                let cell = document.createElement('div')
                cell.classList.add('cell')
                cell.setAttribute('data-cell', j)

                // set first disc game
                if(
                    i + 1 === this.size / 2 && j + 1 === this.size / 2 ||
                    i + 1 === this.size / 2 && j === this.size / 2
                ){
                    let disc = document.createElement('disc')
                    disc.classList.add('disc')
                    disc.classList.add('disc-white')
                    cell.append(disc)
                }

                if(
                    i === this.size / 2 && j + 1 === this.size / 2 ||
                    i === this.size / 2 && j === this.size / 2
                ){
                    let disc = document.createElement('disc')
                    disc.classList.add('disc')
                    disc.classList.add('disc-black')
                    cell.append(disc)
                }

                row.append(cell)
                rows.push(cell)
            }
            this.board.push(rows)
            board.append(row)
        }
    }
}