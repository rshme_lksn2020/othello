class Game{
    constructor(){
        this.board = new Board()
        this.cells = sel('.cell')
        this.player = ''
        this.username = 'Player'
        this.gameover = false
        this.init()
    }

    init(){
        this.listener()
    }

    listener(){
        let that = this

        // create disc when cell is clicked
        this.cells.forEach(cell => {
            cell.addEventListener('click', function(){
               let cell = this.getAttribute('data-cell')
               let row = this.parentElement.getAttribute('data-row')

                    if(that.board.board[row][cell].children.length < 1){
                        let disc = document.createElement('disc')
                        disc.classList.add('disc')
                        disc.classList.add(`disc-${that.player}`)

                        that.board.board[row][cell].append(disc)
                    }

                let cells = that.board.board.flat(2).filter(cell => cell.children.length < 1)

                if(cells.length < 1){
                    alert('Gameover')
                    that.gameover = true
                    gameplay.classList.remove('active')
                    intro.classList.add('active')
                }
            })
        })

        // input player listener
        playerName.addEventListener('keyup', function(){
            if(this.value === ''){
                btnStart.classList.add('disabled')
                btnStart.disabled
            }
            else{
                btnStart.classList.remove('disabled')
                btnStart.disabled = false
            }
        })

        discs.forEach(disc => {
            disc.addEventListener('change', function(){
                that.player = this.value
            })
        })

        btnStart.addEventListener('click', () => {
            if(that.player === ''){
                alert('Choose the disc first !')
            }
            else{
                sel('#player-name')[0].innerText = playerName.value
                intro.classList.remove('active')
                gameplay.classList.add('active')
            }
        })
    }
}